class CreateComment1s < ActiveRecord::Migration
  def change
    create_table :comment1s do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
