json.array!(@comment1s) do |comment1|
  json.extract! comment1, :id, :title, :body
  json.url comment1_url(comment1, format: :json)
end
